import React, { useState } from 'react';
import Task from './Task';

function TodoApp() {
  const [tasks, setTasks] = useState([]);
  const [inputText, setInputText] = useState('');

  const handleInputChange = (event) => {
    setInputText(event.target.value);
  };

  const addTask = () => {
    if (inputText.trim() !== '') {
      const newTask = {
        id: Math.random().toString(36).substr(2, 9),
        text: inputText,
        completed: false,
      };
      setTasks([...tasks, newTask]);
      setInputText('');
    }
  };

  const toggleTaskStatus = (taskId) => {
    const updatedTasks = tasks.map((task) =>
      task.id === taskId ? { ...task, completed: !task.completed } : task
    );
    setTasks(updatedTasks);
  };

  const deleteTask = (taskId) => {
    const updatedTasks = tasks.filter((task) => task.id !== taskId);
    setTasks(updatedTasks);
  };

  const editTask = (taskId, newText) => {
    const updatedTasks = tasks.map((task) =>
      task.id === taskId ? { ...task, text: newText } : task
    );
    setTasks(updatedTasks);
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <h1>ToDo List</h1>
      <input
        type="text"
        value={inputText}
        onChange={handleInputChange}
        placeholder="Add a new task"
      />
      <button onClick={addTask} style={{ backgroundColor: 'blue', color: 'white' }}>Add Task</button>

      <ul>
        {tasks.map((task) => (
          <Task
            key={task.id}
            task={task}
            toggleTaskStatus={toggleTaskStatus}
            deleteTask={deleteTask}
            editTask={editTask} // Hier wird die editTask-Funktion als Prop übergeben
          />
        ))}
      </ul>
    </div>
  );
}

export default TodoApp;
