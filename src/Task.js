import React, { useState } from 'react';

function Task({ task, toggleTaskStatus, deleteTask, editTask }) {
  const [editing, setEditing] = useState(false);
  const [editedText, setEditedText] = useState(task.text);
  const [backgroundColor, setBackgroundColor] = useState('#ffffff'); // Startfarbe

  // Liste von Farben
  const colors = ['#ff9999', '#99ff99', '#9999ff', '#ffff99', '#99ffff'];

  const handleEditInputChange = (event) => {
    setEditedText(event.target.value);
  };

  const handleEditTask = () => {
    if (editedText.trim() !== '') {
      editTask(task.id, editedText);
      setEditing(false);
    }
  };

  const changeBackgroundColor = () => {
    const randomIndex = Math.floor(Math.random() * colors.length);
    setBackgroundColor(colors[randomIndex]);
  };

  return (
    <li style={{ backgroundColor: backgroundColor }}>
      {editing ? (
        <>
          <input
            type="text"
            value={editedText}
            onChange={handleEditInputChange}
          />
          <button onClick={handleEditTask}>Save</button>
        </>
      ) : (
        <>
          <input
            type="checkbox"
            checked={task.completed}
            onChange={() => toggleTaskStatus(task.id)}
          />
          <span style={{ textDecoration: task.completed ? 'line-through' : 'none' }}>
            {task.text}
          </span>
          <button onClick={() => setEditing(true)}>Edit</button>
          <button onClick={() => deleteTask(task.id)}>Delete</button>
        </>
      )}
      <button onClick={changeBackgroundColor}>Change Color</button>
    </li>
  );
}

export default Task;


