function changeBackground() {
    var rootElement = document.getElementById('root');
    var randomColor = Math.random() < 0.5 ? 'lightblue' : 'lightgreen'; // Zufällige Farbwahl
    rootElement.style.backgroundColor = randomColor; // Ändern der Hintergrundfarbe
  }
  
  // Aufruf der Funktion, um den Hintergrund beim Laden der Seite zu ändern (optional)
  window.onload = changeBackground;
  