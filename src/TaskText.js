import React from 'react';

function TaskText({ text, completed }) {
  return (
    <span style={{ textDecoration: completed ? 'line-through' : 'none' }}>
      {text}
    </span>
  );
}

export default TaskText;
