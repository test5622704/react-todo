import React from 'react';
import TodoApp from './TodoApp';

function App() {
  return (
    <div data-testid="app-container">
      <TodoApp />
    </div>
  );
}

export default App;
